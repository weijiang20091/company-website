import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'be Stable',
    Svg: require('@site/static/img/algmon.docusaurus.stability.2.svg').default,
    description: (
      <>
        Supported by the Algmon next-gen digital brain kernel
      </>
    ),
  },
  {
    title: 'at Scale',
    Svg: require('@site/static/img/algmon.docusaurus.core.1.svg').default,
    description: (
      <>
        Supported by the Algmon next-gen PositiveFlow DL toolkit
      </>
    ),
  },
  {
    title: 'in Intelligent',
    Svg: require('@site/static/img/algmon.docusaurus.fast.0.svg').default,
    description: (
      <>
        Carefully guided by the harmony living rule from the universe 
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
