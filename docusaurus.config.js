// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Algmon 算法妈妈',
  tagline: "the advanced Metaverse & Digital Human core tech provider",
  url: 'https://algmon.com/',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'algmon', // Usually your GitHub org/user name.
  projectName: 'company-website', // Usually your repo name.

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          // editUrl: 'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          // editUrl: 'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'Algmon',
        logo: {
          alt: 'My Site Logo',
          src: 'img/logo.svg',
        },
        items: [
          {
            type: 'doc',
            docId: 'news',
            position: 'left',
            label: 'News',
          },
          {
            type: 'doc',
            docId: 'tgif',
            position: 'left',
            label: 'TGIF',
          },
          {
            type: 'doc',
            docId: 'research',
            position: 'left',
            label: 'Research',
          },
          {
            type: 'doc',
            docId: 'product',
            position: 'left',
            label: 'Products',
          },
          {
            type: 'doc',
            docId: 'solution',
            position: 'left',
            label: 'Solutions',
          },
          {
            type: 'doc',
            docId: 'contacts',
            position: 'right',
            label: 'Contacts',
          },
          {
            type: 'doc',
            docId: 'patent',
            position: 'left',
            label: 'Patents',
          },
          {
            type: 'doc',
            docId: 'board',
            position: 'right',
            label: 'Investor Relations',
          },
          {
            type: 'doc',
            docId: 'about',
            position: 'right',
            label: 'About US',
          },
          {
            type: 'doc',
            docId: 'join',
            position: 'right',
            label: 'Join US',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Algmon Doc',
            items: [
              {
                label: '00 Intro',
                to: '/docs/doc/intro',
              },
              {
                label: '01 Advice',
                to: '/docs/doc/advice',
              },
              {
                label: '02 Coding Standard',
                to: '/docs/doc/coding',
              },
              {
                label: '03 VH Intro',
                to: '/docs/product/intro',
              },
              {
                label: '04 VH QA',
                to: '/docs/product/qa',
              },
              {
                label: '05 VH Terminology',
                to: '/docs/product/terminology',
              },
              {
                label: '03 Privacy',
                to: '/docs/docs/privacy',
              },
              {
                label: '04 Terms',
                to: '/docs/docs/terms',
              },
            ],
          },
          {
            title: 'Algmon Code',
            items: [
              {
                label: '00 Intro',
                to: '/docs/code/intro',
              },
              {
                label: '01 Code Snippet',
                to: '/docs/code/snippet',
              },
              {
                label: '02 Command Snippet',
                to: '/docs/code/cmd',
              },
            ],
          },
          {
            title: 'Algmon Brain',
            items: [
              {
                label: '00 PositiveFlow Toolkit',
                to: '/docs/brain/intro',
              },
              {
                label: '01 the Chip',
                to: '/docs/brain/chip',
              },
            ],
          },
          {
            title: 'Algmon Education',
            items: [
              {
                label: '00 Intro',
                to: '/docs/education/intro',
              },
              {
                label: '01 Recurrent NN',
                to: '/docs/education/rnn',
              },
              {
                label: '02 Convolutional NN',
                to: '/docs/education/cnn',
              },
              {
                label: '03 Deep Generative Model',
                to: '/docs/education/dgm',
              },
              {
                label: '04 Reinforcement Learning',
                to: '/docs/education/rl',
              },
              {
                label: '05 New Frontiers',
                to: '/docs/education/nf',
              },
              {
                label: '06 Uncertainty',
                to: '/docs/education/eu',
              },
              {
                label: '07 Bias and Fairness',
                to: '/docs/education/bf',
              },
              {
                label: '08 Information Extraction',
                to: '/docs/education/ie',
              },
              {
                label: '09 Domain Adaptation',
                to: '/docs/education/da',
              },
              {
                label: '10 Higher Dimension Modeling',
                to: '/docs/education/hd',
              },
            ],
          },
          {
            title: 'Algmon Core Partners',
            items: [
              {
                label: '00 Uneeq',
                href: 'https://digitalhumans.com/',
              },
              {
                label: '01 Google',
                href: 'https://www.google.com',
              },
              {
                label: '02 MIT',
                href: 'https://www.mit.edu',
              },
              {
                label: '03 NYU',
                href: 'https://www.nyu.edu',
              },
              {
                label: '04 USC',
                href: 'https://www.usc.edu',
              },
              {
                label: '05 Meta',
                href: 'https://www.meta.com',
              },
              {
                label: '06 Apple',
                href: 'https://www.apple.com',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Algmon. Built with Big LOVE.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
