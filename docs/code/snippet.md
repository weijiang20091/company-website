---
sidebar_position: 1
title: Code Snippet
---

```bash
# check tensorflow version
python -c "import tensorflow as tf; print(tf.version.GIT_VERSION, tf.version.VERSION)"
```

```python
# Simple implementation using Tensorflow
tf.keras.layers.SimpleRNN(rnn_units)
```

```python
# RNNs from scratch using Tensorflow
class MyRNNCell(tf.keras.layers.Layer):
    def __init__(self, rnn_units, input_dim, output_dim):
        super(MyRNNCell, self).__init__()

        # Initialize weight matrices
        self.W_xh = self.add_weight([rnn_units, input_dim]) # TODO: UNDERSTAND deeper
        self.W_hh = self.add_weight([rnn_units, rnn_units]) # TODO: UNDERSTAND deeper
        self.W_hy = self.add_weight([output_dim, rnn_units]) # TODO: UNDERSTAND deeper

        # Initialize hidden state to zeros
        self.h = tf.zeros(run_units, 1)

    def call(self, x):
        # Update the hidden state
        self.h = tf.math.tahh( self.W_hh * self.h + self.W_xh * x)

        # Compute the output
        output = self.W_hy * self.h

        # Return the current output and hidden state
        return output, self.h
```


```python
my_rnn = RNN()
hidden_state = [0, 0, 0, 0]
sentence = ["I", "love", "recurrent", "neural"]
for word in sentence:
    prediction, hidden_state = my_rnn(word, hidden_state)
next_word_prediction = prediction
# >>> "networks!"
```

```python
# simple implementation for nerual network in Tensorflow
import tensorflow as tf
model = tf.keras.Sequential([...])
# pick your favorite optimizaer
optimizer = tf.keras.optimizer.SGD()
while True: # loop forever
    # forward pass through the network
    prediction = model(x)
    with tf.GradientTape() as tape:
        # compute the loss
        loss = compute_loss(y, prediction)
    # update the weights using the gradient
    grads = tape.gradient(loss, model.trainable_variables) # TODO: UNDERSTAND it further
    optimizer.apply_gradients(zip(grads, model.trainable_variables)) # TODO: UNDERSTAND it further
```

```python
# Simple Tensorflow Implementation for gradient descent alg.
import tensorflow as tf
weights = tf.Variable([tf.random.normal()])
while True: # loop forever
    with tf.GradientType() as g:
        loss = compute_loss(weights)
        gradient = g.gradient(loss, weights)
    weights = weights - lr * gradient
```

```python
# Dense Layers Creation in Tensorflow
import tensorflow as tf

model = tf.keras.Sequential([
    tf.keras.layers.Dense(n1),
    tf.keras.layers.Dense(n2),
    ...
    tf.keras.layers.Dense(2)
])
```

```python
# Dense Layer from Scratch
# USE Tensorflow
class MyDenseLayer(tf.keras.layers.Layer):
    def __init__(self, input_dim, output_dim):
        super(MyDenseLayer, self).__init__()

        # Initialize weights and bias
        self.W = self.add_weights([input_dim, output_dim])
        self.b = self.add_weights([1, output_dim])

    def call(self, inputs):
        # Forward propagate the inputs
        z = tf.matmul(inputs, self.W) + self.b

        # Feed through a non-linear activation
        output = tf.math.sigmod(z)

        return output
```

```python
# USE PyTorch
# simple np.mean(...) example
Yval = np.array([0,0,1,1])
Yval_predict = np.array([0,0,1,0])
Yval == Yval_predict
# array([ True,  True,  True, False])
np.mean(Yval == Yval_predict)
# 0.75
```
