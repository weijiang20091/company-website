---
sidebar_position: 1
---
# High Dimension Modeling

## Definition of Higher Dimension

* Number of dimensions >= 4
* 4D = 3D + Time
* 5D = 3D + Time + Scene Adaption Dimension 1
* 6D = 3D + Time + Scene Adaption Dimension 1 & 2
* 7D = 3D + Time + Scene Adaption Dimension 1 & 2 & 3
* 8D = 3D + Time + Scene Adaption Dimension 1 & 2 & 3 & 4

## Low Dimension Modeling - 1D

## Low Dimension Modeling - 2D

## Low Dimension Modeling - Algmon 智能3D量体裁衣

![](./pics/hd/algmon.virtual.fashon.0.png)

* virtual fashion - from 100% designer effect to 60% machine + 40% designer in the context of virtual fashion modeling

![](./pics/hd/algmon.virtual.fashon.1.png)

* virtual fashion - texture modeling
