---
sidebar_position: 1
---

# Lesson 1 - Recurrent Neural Networks (RNN)

![](./pics/rnn/algmon.rnn.00.png)

* audio data is one kind of sequential data

![](./pics/rnn/algmon.rnn.01.png)

* Sequence Modeling Applications
* One to One: Binary Classiticaiton
* Many to One: Sentiment Classification
* One to Many: Image Captioning
* Many to Many: Machine Translation

![](./pics/rnn/algmon.rnn.02.png)

* From 3D to 4D, we have a new dimension called the TIME

![](./pics/rnn/algmon.rnn.03.png)

* Action of the intelligent agent at current time is based on:
* (1) Current Env and 
* (2) The history he/she has observed and learned

![](./pics/rnn/algmon.rnn.04.png)

* Simple recurrence formula to model relationship among
1. The current input
2. The past memory and
3. The expected output
* Note: somehow NAIVE from a higher dimension point of view

![](./pics/rnn/algmon.rnn.05.png)

* h stands for history and being stored in cell memories

![](./pics/rnn/algmon.rnn.06.png)

* Simple implementation using the Tensorflow toolkit

![](./pics/rnn/algmon.rnn.07.png)

* RNNs: Computational graphs across the TIME
![](./pics/rnn/algmon.rnn.08.png)

* Good

![](./pics/rnn/algmon.rnn.09.png)

* To model sequence, we need to
1. Handle variable-length sequence
2. Track long-term dependencies
3. Maintain info about ORDER
4. Share parameters across the sequence
* RNNs meets the above design criteria

![](./pics/rnn/algmon.rnn.10.png)

* mapping vocabulary to a fixed number OR a certain kind of embedding

![](./pics/rnn/algmon.rnn.11.png)

* Actually, we NOT only need info from the past BUT also info from the future in order to guide our current bahaviours and actions

![](./pics/rnn/algmon.rnn.12.png)

* Good

![](./pics/rnn/algmon.rnn.13.png)

* backpropagation in feed-forward models

![](./pics/rnn/algmon.rnn.14.png)

* Taking consideration into the time dimension, every step counts. In regard to what Steve Job is saying: You need to connect your dots when you look back. That's NOT true. The future is already there in higher dimension, and we just simply do NOT realize their existence

![](./pics/rnn/algmon.rnn.15.png)

* Standard RNN Gradient Flow: Exploding Gradients

![](./pics/rnn/algmon.rnn.16.png)

* Standard RNN Gradient Flow: Vanishing Gradients
 
![](./pics/rnn/algmon.rnn.17.png)

* Good

![](./pics/rnn/algmon.rnn.18.png)

* USE ReLU as the default activation function

![](./pics/rnn/algmon.rnn.19.png)

* Init weights to identity matrix for parameter initilization

![](./pics/rnn/algmon.rnn.20.png)

* Use a more complex recurrent unit with gates (gated cell, say LSTM and GRU)
* Long Short Term Memory (LSTMS) networks rely gates to track info among those time steps

![](./pics/rnn/algmon.rnn.21.png)

* repeated modules contain a simple computation node

![](./pics/rnn/algmon.rnn.22.png)

* LSTM cells are able to track information throughout many timesteps

![](./pics/rnn/algmon.rnn.23.png)

* Info has been added or removed

![](./pics/rnn/algmon.rnn.24.png)

* key OPs
1. forget: gate gets rid of irrelevant info

![](./pics/rnn/algmon.rnn.25.png)

* key OPs
1. forget: gate gets rid of irrelevant info
2. store: relevant info from current input
3. update
4. output

![](./pics/rnn/algmon.rnn.26.png)

* the red arrow line: Uninterrupted gradient flow

![](./pics/rnn/algmon.rnn.27.png)

* LSTMs: Key Concepts
1. Maintain a seperate cell state
2. Use gates to control the flow of inforamtion
3. Backpropagation through time with uninterrupted gradient flow

![](./pics/rnn/algmon.rnn.28.png)

* TODO: music generation for the so called unfinished symphony

![](./pics/rnn/algmon.rnn.29.png)

* sentiment analysis for good or bad

![](./pics/rnn/algmon.rnn.31.png)

* encoder & decoder

![](./pics/rnn/algmon.rnn.32.png)

* machine translation issues
* encoding bottleneck
* slow and no parallelization
* no long memory

![](./pics/rnn/algmon.rnn.34.png)

* Good

![](./pics/rnn/algmon.rnn.35.png)

* Good

![](./pics/rnn/algmon.rnn.36.png)

* Good
* RNNs -> LSTMs -> APPs like music generation
