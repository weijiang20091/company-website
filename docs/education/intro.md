---
sidebar_position: 1
---
# Universe & Brain & Others intro

## Universe Intro

![](./pics/algmon.Universe.00.png)

* 0 1 2 3 4 5
* Black Hole

![](./pics/algmon.Universe.01.png)

* 0 1 2 3 4 5
* Attraction

![](./pics/algmon.Universe.02.png)

* 0 1 2 3 4 5
* Photo of Blackhole

![](./pics/algmon.Universe.03.png)

* 0 1 2 3 4 5
* the position for possible life

![](./pics/algmon.Universe.04.png)

* 0 1 2 3 4 5
* the function for predicting collapse

## Brain Intro

![](./pics/algmon.Brain.0.png)

* 0 1 2 3 4 5
* shallow / middle / deep learning

![](./pics/algmon.Brain.1.png)

* 0 1 2 3 4 5
* Good but NOT enough, machine do feature extraction from down to top while human do feature extraction following a top-down approach. Please note that the above observation may vary from individual to individuals.

![](./pics/algmon.Brain.2.png)

* 0 1 2 3 4 5
* Propagation is NOT only forwarding but also to different directions in our brain

![](./pics/algmon.Brain.3.png)

* 0 1 2 3 4 5
* USE Relu in practice for most of the cases

![](./pics/algmon.Brain.4.png)

* 0 1 2 3 4 5
* The purpose of activation function is to introduce non-linearities into the network

![](./pics/algmon.Brain.5.png)

* 0 1 2 3 4 5
* the perceptron example

![](./pics/algmon.Brain.6.png)

* 0 1 2 3 4 5
* Single Perceptron

![](./pics/algmon.Brain.7.png)

* 0 1 2 3 4 5
* Multiple Perceptron

![](./pics/algmon.Brain.8.png)

* 0 1 2 3 4 5
* Can we propose an improved method to help humans to do better in language learning?

![](./pics/algmon.Brain.9.png)

* 0 1 2 3 4 5
* Good Professor with a new & good book published

![](./pics/algmon.Brain.10.png)

* 0 1 2 3 4 5
* A is short for Artificial Intelligence
* B is short for Brain Theory
* C is short for Cognitive Science

![](./pics/algmon.Brain.11.png)

* 0 1 2 3 4 5
* human brain & other brains

![](./pics/algmon.Brain.12.png)

* 0 1 2 3 4 5
* the Book's 10 Chapters

1. Brains in bodies - the social, built and natural environment
2. An action-oriented perspective on space and affordances
3. A look at vision, and a touch more
4. Atmosphere, affordances, and emotion
5. From empathy to mirror neurons and back to aesthetics
6. From libraries to wayfinding, waylosing, and symbolism
7. When buildings have "brains"
8. Evolving the architecture-ready brain
9. Experience and Design: Case Studies
10. Experience and Design: Brining in the brain

![](./pics/algmon.Brain.13.png)

* 0 1 2 3 4 5
* TODO:

![](./pics/algmon.Brain.14.png)

* 0 1 2 3 4 5
* Even Perceptron Involves Construction
* Perceptron will also invlove the Mental Construction, Prof Michael believe

![](./pics/algmon.Brain.15.png)

* 0 1 2 3 4 5
* fully connected hidden layer with one n* and related weights as an example

![](./pics/algmon.Brain.16.png)

* 0 1 2 3 4 5
* a toy example with fully connected dense layer

![](./pics/algmon.Brain.17.png)

* 0 1 2 3 4 5
* When predicting a real number, USE mean square error as the default loss function
* Further design of loss function may be necessary due to adapting to different concret scenes

![](./pics/algmon.Brain.18.png)

* 0 1 2 3 4 5
* simple 2 features GD alg. illustration

![](./pics/algmon.Brain.19.png)

* 0 1 2 3 4 5
* Gradient Descent Alg
* Left: Pesudo Code
* Right: Tensorflow Implementation

![](./pics/algmon.Brain.20.png)

* 0 1 2 3 4 5
* Core OP during training: Backpropagation

![](./pics/algmon.Brain.21.png)

* 0 1 2 3 4 5
* Neural Network in Practice: Optimization

![](./pics/algmon.Brain.22.png)

* 0 1 2 3 4 5
* the loss landscape of neural nets, Dec 2017

![](./pics/algmon.Brain.23.png)

* 0 1 2 3 4 5
* typical gradient descent algs & how to CALL from Tensorflow

1. SGD,      tf.keras.optimization.SGD
2. Adam,     tf.keras.optimization.Adam
3. Adadelta, tf.keras.optimization.Adadelta
4. Adagrad,  tf.keras.optimization.Adagrad
5. RMSProp,  tf.keras.optimization.RMSProp

![](./pics/algmon.Brain.24.png)

* 0 1 2 3 4 5
* OLD technique: Compute with every data point for each iteration
* Current mini-batches based technuqie:

1. More accurate estimation of gradient: Smoother Coverage & Allows for larger learning rates
2. Mini-batches lead to fast training: Can parallelize computation & Achieve significant speed increase on GPU's

![](./pics/algmon.Brain.25.png)

* 0 1 2 3 4 5
* The Problem of Overfitting

![](./pics/algmon.Brain.26.png)

* 0 1 2 3 4 5
* The Solution 1: Dropout

![](./pics/algmon.Brain.27.png)

* 0 1 2 3 4 5
* The Solution 2: Early Stopping

![](./pics/algmon.Brain.28.png)

* 0 1 2 3 4 5
* Core Fundation Overview

1. The Perceptron
2. Neural Networks
3. Training in Practice

## Others

![](./pics/algmon.education.intro.others.00.png)

* 0 1 2 3 4 5
* I/O

![](./pics/algmon.education.intro.others.01.png)

* 0 1 2 3 4 5
* semantics

![](./pics/algmon.education.intro.others.02.png)

* 0 1 2 3 4 5
* basic OPs
* remainder & power OP

![](./pics/algmon.education.intro.others.03.png)

* 0 1 2 3 4 5

![](./pics/algmon.education.intro.others.04.png)

* 0 1 2 3 4 5

![](./pics/algmon.education.intro.others.05.png)

* 0 1 2 3 4 5

![](./pics/algmon.education.intro.others.06.png)

* 0 1 2 3 4 5

## References

![](./pics/algmon.MIT.education.cover.png)

* MIT Deep Learning 6.S191
* http://introtodeeplearning.com/
* 
* We thanks MIT for the gentle support
* academic contact: Alexander Amini
* https://www.mit.edu/~amini/

![](./pics/algmon.standford.education.cover.png)

* Standford U Computer Vision cs231n
* http://cs231n.stanford.edu/
* 
* We thanks Standford U for the gentle support
* academic contact: Feifei Li
* https://profiles.stanford.edu/fei-fei-li
