---
sidebar_position: 1
---
# Lecutre 04 - Reinforcement Learning

## PPTs

![](./pics/rl/algmon.edu.rl.00.png)

* 0 1 2 3 4 5
* agent performs learning in dynamic environment

![](./pics/rl/algmon.edu.rl.01.png)

* 0 1 2 3 4 5
* example like robotics

![](./pics/rl/algmon.edu.rl.02.png)

* 0 1 2 3 4 5
* case: Alpha Star

![](./pics/rl/algmon.edu.rl.03.png)

* 0 1 2 3 4 5
* classes of learning problems
* RL
* Data: state-action pairs
* Goal: Maximize future rewards over many time steps

![img](./pics/rl/algmon.edu.rl.04.png)

* 0 1 2 3 4 5
* actions
* in action space

![img](./pics/rl/algmon.edu.rl.05.png)

* 0 1 2 3 4 5
* observations
* then reward

![img](./pics/rl/algmon.edu.rl.06.png)

* 0 1 2 3 4 5
* agent -> metaverse: action
* metaverse -> agent: observe
* agent & metaverse: interaction

![](./pics/rl/algmon.edu.rl.17.png)

* 0 1 2 3 4 5
* reward can be seen as a formula

![](./pics/rl/algmon.edu.rl.18.png)

* 0 1 2 3 4 5
* Q-function
* expected total future reward
* state s
* action a

![](./pics/rl/algmon.edu.rl.19.png)

* 0 1 2 3 4 5
* policy
* strategy

![](./pics/rl/algmon.edu.rl.20.png)

* 0 1 2 3 4 5
* value
* policy

![](./pics/rl/algmon.edu.rl.21.png)

* 0 1 2 3 4 5

![](./pics/rl/algmon.edu.rl.22.png)

* 0 1 2 3 4 5

![](./pics/rl/algmon.edu.rl.23.png)

* 0 1 2 3 4 5

![](./pics/rl/algmon.edu.rl.24.png)

* 0 1 2 3 4 5
* learn Q-function
* infer optimal policy

![](./pics/rl/algmon.edu.rl.25.png)

* 0 1 2 3 4 5
* rank by reward score

![](./pics/rl/algmon.edu.rl.26.png)

* 0 1 2 3 4 5

![](./pics/rl/algmon.edu.rl.27.png)

* 0 1 2 3 4 5

![](./pics/rl/algmon.edu.rl.28.png)

* 0 1 2 3 4 5
* policy gradient methods

![](./pics/rl/algmon.edu.rl.30.png)

* 0 1 2 3 4 5

![](./pics/rl/algmon.edu.rl.31.png)

* 0 1 2 3 4 5
* policy gradient (PG) can model contunuous action space

![](./pics/rl/algmon.edu.rl.32.png)

* 0 1 2 3 4 5

![](./pics/rl/algmon.edu.rl.33.png)

* 0 1 2 3 4 5

![](./pics/rl/algmon.edu.rl.34.png)

* 0 1 2 3 4 5

![](./pics/rl/algmon.edu.rl.35.png)

* 0 1 2 3 4 5

![](./pics/rl/algmon.edu.rl.36.png)

* 0 1 2 3 4 5

![](./pics/rl/algmon.edu.rl.37.png)

* 0 1 2 3 4 5

![](./pics/rl/algmon.edu.rl.38.png)

* 0 1 2 3 4 5
* Reinforcement Learning in real life
* Training Algorithm
1. Initialize the agent
2. (difficult part) Run a policy until termination
3. Record all states, actions and rewards
4. Decrease probability of actions that resulted in low reward
5. Increase probability of actions that resulted in high reward

![](./pics/rl/algmon.edu.rl.39.png)

* 0 1 2 3 4 5
* VISTA 2.0: Photorealistic and high-fidelity simulator for training and testing self-driving cars

![](./pics/rl/algmon.edu.rl.40.png)

* 0 1 2 3 4 5
* Deploying end-to-end RL for autonomous vehicles
* policy gradient RL agent trained entirely within VISTA simular

![](./pics/rl/algmon.edu.rl.41.png)

* 0 1 2 3 4 5
* Deep Reinforcement Learning Applications

![](./pics/rl/algmon.edu.rl.42.png)

* 0 1 2 3 4 5
* Aim: Get more board territory than your opponent
* important parameters includes (1) board size (2) positions (3) % legal and (4) legal positions
* greator number of legal board positions than atoms in the universe

![](./pics/rl/algmon.edu.rl.43.png)

* 0 1 2 3 4 5
* AlphaGo Beats Top Human Player at Go (2016)
* step1: init training human data
* step2: self play and reinforcement learning
* step3: intution about board state

![](./pics/rl/algmon.edu.rl.44.png)

* MuZero: Learning dynamics for planning (2020)
* MuZero: No Knowledge at all at the first sight

![](./pics/rl/algmon.edu.rl.case.google.tpu.00.png)

* 0 1 2 3 4 5
* Chip Design with Deep Reinforcement Learning
* A graph neural network generates embeddings that are concatenated with the metadata embeddings to form the input to the policy and value networks.

![](./pics/rl/algmon.edu.rl.case.google.tpu.01.png)

* 0 1 2 3 4 5
* During each training iteration, the macros are placed by the policy one at a time and the standard cell clusters are placed by a force-directed method. The reward is calculated from the weighted combination of approximate wirelength and congestion.

![](./pics/rl/algmon.edu.rl.case.google.tpu.02.png)

* 0 1 2 3 4 5
* Macro placements of Ariane, an open-source RISC-V processor, as training progresses. On the left, the policy is being trained from scratch, and on the right, a pre-trained policy is being fine-tuned for this chip. Each rectangle represents an individual macro placement. Notice how the cavity discovered by the from-scratch policy is already present from the outset in the pre-trained policy’s placement.

![](./pics/rl/algmon.edu.rl.case.google.tpu.03.png)

* 0 1 2 3 4 5
* Convergence plots for two policies on Ariane blocks. One is training from scratch and the other is finetuning a pre-trained policy.

![](./pics/rl/algmon.edu.rl.case.google.tpu.04.png)

* 0 1 2 3 4 5
* Training data size vs. fine-tuning performance.

## References

* Algmon Education Intro https://algmon.com/docs/education/intro
* MIT Deep Learning 6.S191 http://introtodeeplearning.com/
* Google AI Blog https://ai.googleblog.com/
