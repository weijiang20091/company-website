---
sidebar_position: 1
---
# Lesson2 - Convolutional Neural Networks (CNN)

![](./pics/cnn/algmon.cnn.000.png)

* 0
* Vision is one of the most important sensing modules for both human & machine

![](./pics/cnn/algmon.cnn.001.png)

* 0
* the CNN based core tech can, in a certain degree, understand the semantics among ojbects, including persons, things etc.

![](./pics/cnn/algmon.cnn.002.png)

* 0
* the Rise and Impact of computer vision by segments
  * Robotics
  * Accessibility
  * Biology
  * Mobile Computing
  * Autonomous Drving etc.

![](./pics/cnn/algmon.cnn.003.png)

* 0
* Facial Detection & Recognition General Workflow
* Detect Keypoints & Dots in 2D plane -> Connect the Related Dots -> Recognize Typical Shapes in the face region based on Observation -> Machine now can somehow remember the human & animal's face related region
* 
* Face Modeling
* Triangle based modeling

![](./pics/cnn/algmon.cnn.00.png)

* 0

![](./pics/cnn/algmon.cnn.01.png)

* 0

![](./pics/cnn/algmon.cnn.02.png)

* 0

![](./pics/cnn/algmon.cnn.03.png)

* 0
* Regression & Classification
* Predict Real Numbers & Probablity Distribution based on Softmax like Loss Functions

![](./pics/cnn/algmon.cnn.04.png)

* 0
* Classes
  * Person: Nose, Eyes and Mouth
  * Car: Wheels, License Plate and Headlights
  * House: Door, Windows and Steps

![](./pics/cnn/algmon.cnn.05.png)

* 0
* Manual Feature Extraction
  * DECIDE some typical domian knowledge
  * DEFINE useful features
  * USE features for classify

![](./pics/cnn/algmon.cnn.06.png)

* Hierarchy of Features
  * Low Level Features
    * Edges, dark spots
  * Mid Level Features
    * Eyes, ears, nose
  * High Level Features
    * facial structure

![](./pics/cnn/algmon.cnn.07.png)

![](./pics/cnn/algmon.cnn.08.png)

![](./pics/cnn/algmon.cnn.09.png)

![](./pics/cnn/algmon.cnn.10.png)

![](./pics/cnn/algmon.cnn.11.png)

![](./pics/cnn/algmon.cnn.12.png)

![](./pics/cnn/algmon.cnn.13.png)

![](./pics/cnn/algmon.cnn.14.png)

![](./pics/cnn/algmon.cnn.15.png)

![](./pics/cnn/algmon.cnn.16.png)

![](./pics/cnn/algmon.cnn.17.png)

![](./pics/cnn/algmon.cnn.18.png)

![](./pics/cnn/algmon.cnn.19.png)

![](./pics/cnn/algmon.cnn.20.png)

![](./pics/cnn/algmon.cnn.21.png)

![](./pics/cnn/algmon.cnn.22.png)

![](./pics/cnn/algmon.cnn.23.png)

![](./pics/cnn/algmon.cnn.24.png)

![](./pics/cnn/algmon.cnn.25.png)

![](./pics/cnn/algmon.cnn.26.png)

![](./pics/cnn/algmon.cnn.27.png)

![](./pics/cnn/algmon.cnn.28.png)

![](./pics/cnn/algmon.cnn.29.png)

![](./pics/cnn/algmon.cnn.30.png)

![](./pics/cnn/algmon.cnn.31.png)

![](./pics/cnn/algmon.cnn.32.png)

![](./pics/cnn/algmon.cnn.33.png)

![](./pics/cnn/algmon.cnn.34.png)
