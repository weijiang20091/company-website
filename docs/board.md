---
sidebar_position: 1
title: Investor Relations
---
## 00. Executive Committee

* Jinpei Jiang
* Wei Huang
* Wei Jiang

### Executive Committee Charter

### Purpose

* The Executive Committee of the Board of Directors of Algmon serves as an administrative committee of the Board to facilitate approval of certain corporate actions that do not require consideration by the full Board, as outlined.

### Membership

* The Executive Committee will consist of at least three members of the Board. The members of the Executive Committee will be appointed by and serve at the discretion of the Board.

### Authority

* The Executive Committee will have and may exercise such powers and authority in the management of the business and affairs of Algmon as are specifically delegated to it by resolution of the full Board.

### Meetings

* Meetings of the Executive Committee will be held from time to time as determined by the Board and/or the members of the Executive Committee, in response to the needs of the Board.

### Notes

* The Executive Committee will maintain written notes of its meetings, which will be distributed to the Board at its next meeting following any meeting of the Executive Committee and filed in Algmon’s notebooks along with the notes of the meetings of the Board.
* The above has been adopted since 2022-Q4

## 01. Board of Directors

* Feiyu Wu
* Xuejun Ma
* Heng Huang
* Ping Huang
* Wei Huang
* Jinpei Jiang
* Qiqi Jiang
* Wei Jiang

## 02. Board Committees
## 03. Audit and Compliance Committee
## 04. Compensation Committee
## 05. Nominating Corporate Governance Committee

* Lifeng Jiang
