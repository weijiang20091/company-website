---
sidebar_position: 1
---
# Products

## Algmon Baby ABC App

![](./pics/algmon.app.baby.png)

* Algmon Baby ABC App
* An App designed for babies to play, to create and to learn, with some Algmon Universe & Digital Human related borning things embedded
*
* A for Artificial Intelligence
* B for Brain Theory
* C for Cognitive Science
* Age Range: 0 - 3 Years
* Public Avaiable in Spring 2023
*
* For more, please contact
* consult@algmon.com

## Algmon OCR Toolkit

![](./pics/algmon.positiveocr.png)

* An OCR toolkit originally developed by baidu and been adopted into the Algmon AI ecosystem
* Great News! We have OPEN Source this project
* SEE https://gitee.com/algmon/IMPROVED-PaddleOCR-for-Algmon-Brain
* 
* For more, please contact
* consult@algmon.com


## Algmon Baby Positivefolow Toolkit

![](./pics/algmon.positiveflow.png)

* a brand new framework & toolkit for deep learning
* Great News! We have OPEN Source this project
* SEE https://github.com/algmon/public/blob/main/src/algmonpositiveflow
*
* tutorials
* main.tutorial.00.py
* main.tutorial.01.py
* main.tutorial.02.py
* main.tutorial.03.ipynb
* main.tutorial.04.py
*
* We have developed an IMPROVED version of Apple Tensorflow for better Algmon Brain Operation Performances
* For the latest update of algmon tensorflow, SEE: https://github.com/algmon/IMPROVED-Tensorflow-for-Algmon-Brain/blob/master/README.md

## Algmon Baby Deep Learning Compiler Toolkit

![](./pics/algmon.deep.learning.compiler.baby.png)

* Algmon Baby Deep Learning Compiler is a compiler stack for deep learning systems. It is designed to close the gap between the productivity-focused deep learning frameworks, and the performance and efficiency-focused hardware backends. The tool works with deep learning frameworks to provide end to end compilation for different backends. In short, a set of useful tools for key operations such as DL network compiling and hyperparameter tuning.
* Public Avaiable in Spring 2023
*
* For more, please contact
* consult@algmon.com

## Algmon Baby Hardware Accelerator Chip

![](./pics/algmon.chip.baby.png)

* Algmon Baby 1 PRO & MAX
* A set of chips for agent to perform actions and get reward from the metaverse
* Public Avaiable in Fall 2023
*
* For more, please contact
* consult@algmon.com
