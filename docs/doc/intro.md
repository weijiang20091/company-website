# Algmon Docs

## Intro

* Algmon Docs provide state-of-the-art bible guidlines for ambitious research & engineering teams to LEARN, to FOLLOW and to MODIFY on their own needs.

## Important Docs Listing

* Intro
* Advice
* Coding Standard
* Useful Commands
