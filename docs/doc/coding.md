---
sidebar_position: 1
---
# Coding Standard 编码标准

## 00 Background

Python & Java & Vue & Swift are the main 4 programming language used at Algmon. This style guide is a list
of *dos and don'ts* for programs.

To help you format your python code correctly, we've created a [settings file for
Vim](python_style.vim). For Emacs, the default settings should be fine.

Many teams use the [yapf](https://github.com/google/yapf/)
auto-formatter to avoid arguing over formatting.

## 01 Dev Guides

* For Intro, SEE https://google.github.io/styleguide/
* Main dev machine recommned: Macbook Pro (After 2022) with Apple Silicon Chip and at least 16G of memory
* Main dev IDEs: vscode & vim
* Main dev IDE Extension: Git Lens, Tabnine, Docker etc.
