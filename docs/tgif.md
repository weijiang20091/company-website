---
title: Thanks GOD, it's Friday.
---
![](./pics/tgif.logo.png)

## TGIF Intro

0. TGIF is an weekly knowledge sharing event hosted company wide and originally been introduced by Google
1. Core Researchers & Engineers Oriented
2. Higher Dimension Space Modeling
3. Multi-modal AI Models for Intelligent Agents & Env

## --> TGIF 5 2022-12-05 5:00PM <--

![](./tgif/pics/tgif.logo.05.png)

* Central Topic: 相融共生 - Mercy
* Speaker: the Algmon Team
* Invited Guest:
* Style: 1 Hour
* Tencent Meeting ID: 156-590-XXX

## TGIF 1 2022-11-04 5:00PM

![](./tgif/pics/tgif.logo.01.png)

* Central Topic: 相融共生 - Overview & Threads
* Speaker: the Algmon Team
* Invited Guest:
* Style: 1 Hour
* Tencent Meeting ID: 156-590-XXX

## TGIF 2 2022-11-11 5:00PM

![](./tgif/pics/tgif.logo.02.png)

* Central Topic: 相融共生 - Product & Scene & Marketing
* Speaker: the Algmon Team
* Invited Guest:
* Style: 1 Hour
* Tencent Meeting ID：766-215-XXX

## TGIF 3 2022-11-18 5:00PM

![](./tgif/pics/tgif.logo.03.png)

* Central Topic: 相融共生 - Dimension UP & DOWN
* Speaker: the Algmon Team
* Invited Guest:
* Style: 1 Hour
* Tencent Meeting ID：820-995-XXX

## TGIF 4 2022-11-25 5:00PM

![](./tgif/pics/tgif.logo.04.png)

* Central Topic: 相融共生 - Self & EchoSystem
* Speaker: the Algmon Team
* Invited Guest:
* Style: 1 Hour
* Tencent Meeting ID: 508-469-XXX

## TGIF 6 2022-12-12 5:00PM

* Central Topic:
* Speaker: the Algmon Team
* Invited Guest:
* Style: 1 Hour
* Tencent Meeting ID:

## TGIF 7 2022-12-19 5:00PM

* Central Topic:
* Speaker: the Algmon Team
* Invited Guest:
* Style: 1 Hour
* Tencent Meeting ID:

## TGIF 8 2022-12-26 5:00PM

* Central Topic:
* Speaker: the Algmon Team
* Invited Guest:
* Style: 1 Hour
* Tencent Meeting ID:
