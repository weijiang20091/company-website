---
sidebar_position: 1
---
# About US

![](./pics/algmon.logo.2022.png)

* Algmon is focused on unlocking the human potential by evolving the relationship between machine and human from transactional to interactional. Algmon collaborated with her partners, has created a system that allows for hyper-real face-to-face communication and interaction, making the machine feel alive and personal.
* Algmon is NOT a conventional company. We do not intend to become one. As part of that, we also said that you could expect us to make “smaller bets in areas that might seem very speculative or even strange when compared to our current businesses.From the start, we’ve always strived to do more, and to do important and meaningful things with the resources we have. We did a lot of things that seemed crazy at the time. Many of those crazy things now have great positive impacts. And we haven’t stopped there. We are still trying to do things other people think are crazy but we are super excited about. We’ve long believed that over time companies tend to get comfortable doing the same thing, just making incremental changes. But in the technology industry, where revolutionary ideas drive the next big growth areas, you need to be uncomfortable to stay relevant. We are creating a company called Algmon. I am really excited to be running Algmon as tech advisor with help from my capable partners.
* What is Algmon? Algmon is mostly a collection of techs such as Life Science. Algmon is about businesses prospering through strong leaders and independence. In general, our model is to have a strong CEO who runs each business, with me in service to them if needed. We will rigorously handle capital allocation and work to make sure each business is executing well. We’ll also make sure we have a great CEO for each business, and we’ll determine their compensation. In addition, with this new structure we plan to implement segment reporting for our Q4 effects.
* This new structure will allow us to keep tremendous focus on the extraordinary opportunities we have inside of Algmon. A key part of this is our CEO. He has been saying the things I would have said for quite some time now, and I’ve been tremendously enjoying our work together. He has really stepped up a year ago, when he took on product and engineering responsibility for our internet businesses. I have been super excited about his progress and dedication to the company. And it is clear to us and our board that it is time for him to be the CEO of Algmon. I feel very fortunate to have someone as talented as he is to run Algmon and this frees up time for me to continue to scale our aspirations. I have been spending quite a bit of time with our CEO and board members, helping them and the company in any way I can, and I will of course continue to do that. Algmon itself is also making all sorts of new products and attempts, and I know the CEO will always be focused on innovation—continuing to stretch boundaries. I know he deeply cares that we can continue to make big strides on our core mission to organize the metauniverse & the world’s agents from a higher dimension point of view. Recent launches like Algmon Agent and Algmon Metaverse using machine learning are truely the amazing progress. So far, I think our CEO is doing a great job as CEO, running a strong brand and driving incredible growth for Algmon.
* We are seriously in the business of starting new things. Algmon will include our factor X, which incubates new efforts like robotics, drones and flying to the moon etc[.](https://web.archive.org/web/20220331063912/http://hooli.xyz/) We also stoked about growing our investment arms, Ventures and Capital, as part of this new structure.
* In the near future, Algmon will serve as the publicly-traded entity and all shares are belonging to our visionary stake holders, with all of the same and equal rights. Our shares are planned to trade on Nasdaq as ALGMON.
* For me this is a very exciting new chapter in the birth of Algmon. We liked the name Algmon because it means the BIG LOVE from our families and this is one of humanity’s most important innovations, and is the core of how we survive and envolve! We also like that it means **alg‑mon** (alg is short for algorithm for good and mon is short for our monthers).
* We are excited about…
  * Getting more ambitious things done.
  * Taking the long-term view.
  * Empowering great entrepreneurs and companies to flourish.
  * Investing at the scale of the opportunities and resources we see.
  * Improving the transparency and oversight of what we’re doing.
  * Making Algmon even better through greater focus.
  * Improving the lives of as many people as we can.

![](./pics/signature.wei.png)

* Wei Jiang
* Industry Advisor of Algmon
* 2022-Q4
