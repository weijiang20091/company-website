---
sidebar_position: 1
title: Algmon Patents
---

## Intro

* Areas
  * artificial intelligence
  * machine learning
  * reinforcement learning
* 
* Number of patents granted till now: ?
* Number of patents pending till now: ?
* 
* For patent offices who want to be a partner with Algmon, please contact via email
* patent@algmon.com

## Related Patents

* US11120365B2
* For hierarchical decomposition deep reinforcement learning for an artificial intelligence model
* US9754221B1
* Processor for implementing reinforcement learning operations
* US20180121766A1
* Enhanced human/machine workforce management using reinforcement learning
* US20190101985A1
* Systems and methods for deep reinforcement learning using a brain-artificial intelligence interface
* CN109947567B
* Multi-agent reinforcement learning scheduling method and system and electronic equipment
