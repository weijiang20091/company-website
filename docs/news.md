---
sidebar_position: 1
title: Algmon News
---

## 2022-11-28

![](./news/pics/algmon.io.2022.png)

* the rough schedule for Algmon I/O 2022 has been proposed.
* Keynote Title: "the Unvierse, the Brain and your Body"
* Keynote Speaker: Jinpei Jiang, CEO of Algmon
* 
* Section 1: Not only forward & backward but also full
* Host: Wei Jiang, Industry Advisor of Algmon
* 
* Section 2: Research and Engineering, the beautiful balance
* Host: Wei Huang, Academic Advisor and Board of Algmon

## 2022-11-18

![](./news/pics/algmon.python.lib.png)

* We are happy to OPEN Source our python packages in draft for anyone to enjoy!
* Any comments & PRs are welcome!
* algmon brain
* https://pypi.org/project/algmonbrain/
* algmon body
* https://pypi.org/project/algmonbody/
* algmon infra
* https://pypi.org/project/algmoninfra/

## 2022-11-15

![](./news/pics/algmon.education.classroom.png)

* We USE Github Classroom to deliver our education materials to our clients
* A typical example for agent & metaverse learning is here
* https://classroom.github.com/a/7e-LVygz

## 2022-11-04

* We OPEN Source our homepage project for developers
* Algmon + developers = harmony living

![](./pics/algmon.homepage.20221104.png)

* Current Algmon Homepage

## 2022-01-01

* Yearly Overall Prediction: Good

![](./pics/algmon.logo.2022.BG.2.png)

## 2021-01-01

* Yearly Overall Prediction: Good

![](./pics/algmon.logo.2022.BG.0.png)

## 2020-03-06

* Algmon Established & OPEN to Public

![](./pics/algmon.company.registration.info.png)
