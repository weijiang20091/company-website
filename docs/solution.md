---
sidebar_position: 1
title: Algmon Tech Solution Series
---

![](./solutions/pics/solution.general.EN.png)

## Intro

* Fit for both tech & marketing people
* FREE version of tech solution is designed for fitting most of the sceneriors and naturally pre-divided by vertical segments
* PAID version of tech solution is designed to be broad & deep and detail enough for B & G customers
* to C: direct trying and expereince the product & service

![](./solutions/pics/toB.steps.to.make.deals.png)

## Pre-Defined Vertical Segments

![](./solutions/pics/digital.human.typical.tech.arch.png)

* Broadcasting & Simulation & QA
* Reinforcement Learning by Algmonbrain & Algmonbody

### 00. Communication

* Alg: Multi Round Conversation
* Scenerio: Intelligent Call Center for Improved Customer Service
* Assumed Target Client: T-Mobile

### 01. Education

* Alg: Higher Dimension Modeling
* Scenerio: Intelligent Virtual Classroom
* Assumed Target Client: gdsyzx 广东实验中学

### 02. Headhunting

* Alg: Multi-Modal Intelligent Agent Sensing & Planning & Acurating
* Scenerio: Virtual Technical & Behavior Interview
* Assumed Target Client: Alphabet Inc. (formally Google)

### 03. Insurance

* Alg: Student-centered Health Risk Modeling & Analysis at scale in real time
* Scenerio: Personal Health Management & Risk Prediction
* Assumed Target Client: Aetna Student Health Privider

### 04. Media

* Alg: 2D Model Transformation & Virtual Human Photography + Video Clip Edition
* Scenerio: Rich contant and 2D virtual human based broadcasting
* Assumed Target Client: the BBC broadcasting network

### 05. Real Estate

* Alg: Higher Dimension Modeling
* Scenerio: Higher Dimension Modeling
* Assumed Target Client: Evergrande

### 06. Other Vertical Segments

* Banking
* Automotive
* Fashion

### Contact US

* consult@algmon.com
