---
sidebar_position: 1
title: Algmon PositiveFlow
---
![](../pics/infra/PositiveFlow.Overview.png)

## Authors & Contributors

* the Algmon engineering team & external developers
* 
* this project is OPEN Source, PRs are welcome!
* algmon github positive-flow base
* https://github.com/algmon/public/tree/main/src/positive-flow
* algmon gitee positive-flow base
* https://gitee.com/algmon/public/tree/master/src/positive-flow

## OPEN Core APIs

* High Level
* https://www.apifox.cn/apidoc/shared-4b5d927b-a761-49df-9ed1-27f3089f6b95/doc-1639959
* Mid Level
* 
* Low Level
* 

## Abstract

* PositiveFlow is the next-generation end-to-end deep learning framework & toolkit designed from ground up, with the purpose to combine the Brain Science and Deep Learning together. We believe that current general purpose DL framework such as Tensorflow, Pytorch and Paddle has been outdated. They are lacking the consideration and connection between the human brain and the machine brain when they train, eval, deploy and predict.

## 1 Introduction

* Our main contributions
  * presentation of PositiveFlow, with empjasis on how we designed it to support multiple tasks of agent without sacrificing simplicity, modularity  and flexibility
  * competitive performance on a variety of human & machine tasks for agents
  * We believe the PositiveFlow toolkit has the potential to significantly accelerate research and innovation in the field of brain science for humans and deep learning for machines

## 2 Related Work

* Vision related
* Speech related
  * Speech
  * ESPnet
  * Transformers
  * Wav2vec
* Language related
* Multi-modal related

## 3 Design Principles

* Accessibility
* Ease of Use
* Replicability

## 4 Architecture

* From an architectural standpoint, PositiveFlow sits in between a library and a framework. Where libraries require users to manage dataflow by calling library-defined functionality, frameworks primarily define a custom lifecycle in which user-defined functionalities are invoked in specific places (inversion of control). Most code in PositiveFlow follows a library-style collection of modular and standalone building blocks, including practical routines for data loading, decoding, signal processing, and other convenient utilities. However the central Agent class and Metaverse class uses inversion of control to define a general training loop. Therefore, PositiveFlow is most accurately described as a toolkit. As shown in Figure 1, the code for training a model is contained within a single Python script. Training begins by calling the script with a set of hyperparameters: TODO: python train.py hparams.yaml . These hyperparameters, declared in human-readable YAML format, contain the location of one or more data manifest files using either CSV or JSON formats. Unlike many other toolkits, PositiveFlow orchestrates experiments in Python directly, without relying on external scripts. This allows code for data loading, modeling, optimization, and evaluation to interact naturally. Moreover, the training script exposes the computations likely to be changed most frequently (e.g., forward computations, data transformations, etc.), making them easy to access and modify. PositveFlow treats the user’s code as a first-class citizen: all code written by the user is treated the same as the core code.

## 5 Results (with typical prediction tasks)

* Vision related
* 
* Speech related
* Speech to Text - PASS
* Speaker Recognition and Diarization - PASS
* Speech Enhancement & Separation - PASS
* Text to Speech - NOT Pass
* Language related
* 
* Mluti-modal related

## 6 Limitations and Future Work

* SUPPORT decoding with Finite State Transducers (FSTs)
* Enhance real-time speech processing
* more ...

## 7 Conclusion

* PositiveFlow is a coordinated effort towards making deep learning & brain science tech and are eager to see where its rapidly growing community of users takes the project in the future.

## 8 References

1. A. Paszke, S. Gross, F. Massa, A. Lerer, J. Bradbury, G. Chanan, T. Killeen, Z. Lin,
   N. Gimelshein, L. Antiga, A. Desmaison, A. Kopf, E. Yang, Z. DeVito, M. Raison, A. Tejani,
   S. Chilamkurthy, B. Steiner, L. Fang, J. Bai, and S. Chintala. Pytorch: An imperative style,
   high-performance deep learning library. In Proc. of NeurIPS, 2019.
2. M. Ravanelli, T. Parcollet, and Y. Bengio. The PyTorch-Kaldi Speech Recognition Toolkit. In
   Proc. of ICASSP, 2019.
3. M. Pariente, S. Cornell, J. Cosentino, S. Sivasankaran, E. Tzinis, J. Heitkaemper, M. Olvera,
   F. Stöter, M. Hu, J. M. Martín-Doñas, D. Ditter, A. Frank, A. Deleforge, and E. Vincent.
   Asteroid: the PyTorch-based audio source separation toolkit for researchers. In Proc. of
   Interspeech, 2020.
4. H. Bredin, R. Yin, J. M. Coria, G. Gelly, P. Korshunov, M. Lavechin, D. Fustes, H. Titeux,
   W. Bouaziz, and M. Gill. pyannote.audio: neural building blocks for speaker diarization. In
   Proc. of ICASSP, 2020.
5. B. Desplanques, J. Thienpondt, and K. Demuynck. ECAPA-TDNN: emphasized channel
   attention, propagation and aggregation in TDNN based speaker verification. In Proc. of
   Interspeech, 2020.
6. C. Subakan, M. Ravanelli, S. Cornell, M. Bronzi, and J. Zhong. Attention is all you need in
   speech separation. In Proc. of ICASSP, 2021.
7. S. Majumdar, J. Balam, O. Hrinchuk, V. Lavrukhin, V. Noroozi, and B. Ginsburg. Citrinet:
   Closing the gap between non-autoregressive and autoregressive end-to-end models for auto-
   matic speech recognition, 2021. arXiv:2104.01721.
8. C. Wang, M. Rivière, A. Lee, A. Wu, C. Talnikar, D. Haziza, M. Williamson, J. Pino, and
   E. Dupoux. Voxpopuli: A large-scale multilingual speech corpus for representation learning,
   semi-supervised learning and interpretation. arXiv:2101.00390, 2021.
9. C. Li, J. Shi, W. Zhang, A. Subramanian, X. Chang, N. Kamo, M. Hira, T. Hayashi, C. Böd-
   deker, Z. Chen, and Shinji Watanabe. Espnet-se: End-to-end speech enhancement and
   separation toolkit designed for ASR integration. In Proc. of the IEEE Spoken Language
   Technology Workshop, 2021.
10. A.Baevski,Y.Zhou,A.Mohamed,andM.Auli.wav2vec2.0:Aframeworkforself-supervised
    learning of speech representations. In Proc. of NeurIPS, 2020.
11. Mirco Ravanelli et. al. SpeechBrain: A General-Purpose Speech Toolkit, 2021.

## Appendix

## A.1 Statement on social impact

## A.2 Performance comparizon with other toolkits

## A.3 Additional Tasks

## A.4 Terminology

* Algmon 算法妈妈
* Grey Matter 灰质
* Cortex 皮质
* White Matter 白质
* Cerebellum 小脑
* Limbic system 边缘系统
* Frontal Lobe 额叶
* Temporal Lobe 颞叶
* Parietal Lobe 顶叶
* Occipital Lobe 枕叶
* Unknown Area 未知区域
* Hippocampus 海马区
* Amygdala 杏仁核
* Attention Mechanism 注意力机制
* Perception 感知
* Sensing 感知
* Hearing 听觉
* Touching 触觉
* Smelling Capability 味觉
* Motion 动作
* Planning 规划
* Visual 视觉
* Capability 能力
* Unknown Sensing Capability 未知觉感知能力
* Long Term Memory 长时记忆
* Middle Term Memory 长短间记忆
* Short Term Memory 短时记忆
* Management 管理
* Emotion 情绪
* Emotion Management 情绪管理
* Machine & Human 人机
* Texture Recognition 材质识别
* Food Tasty Recognition 食物美味度识别
* Reasonable Motion Sequence Generation 合乎常理的动作效应序列输出
* Visual & Ranking Capability 视觉与愿景识别与排序

## A.5 Toolkit Core Functionality Screenshot

![](../pics/infra/PositiveFlow.Viz.png)

* the Algmon Lab module helps researchers & engineers for fast prototyping the DL network
