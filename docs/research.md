---
sidebar_position: 1
---
# Algmon Research

## High Dimensional Modeling

## Deep Learning Applications

## Deep Learning Arch

## Deep Learning Compiler

## Chip Design

![](./pics/research/algmon.research.rl.case.google.tpu.fast.chip.design.abs.png)

* Reference: chip design by google published in nature, 2021
* Core Tech: chip floorplanning
* Goal: automatically produce manufacturable layouts
* Key Metrics: power consumptions, performance and chip area
* Proposed Technique: an edge-based graph convolutional neural network architecture capable of learning rich and transferable representations of the chip
* Conclusion: chip design by machine agent is better than the human agent

![](./pics/research/algmon.research.rl.case.google.tpu.fast.chip.design.hardware.png)

* Scene: the technique is used to design the next generation artificial intelligence (AI) accelerators, and has the potential to save thousands of hours of human agent effort for new generation chip development
* -> the intro
* define the problem: chip placement
* bg: a computer chip is divided into dozens of blocks, each of which is an individual module, such as a memory subsystem, compute unit or control logic system etc
* related work: 3 kinds of approaches for chip floorplanning: (1) partitioning based (2) stochastic / hill-climbing approaches and (3)analytic solvers
* the proposed technique: can scale to netlists with millions of nodes, and optimizes directly for any mixture of differentiable or non-differentiable cost functions. Furthermore, the technique improves in both speed and quality of result because it is exposed to more instances of the chip placement problem
* explanation: chip floorplanning is analoguous to a game
  * with pieces
    * netlist topologies
    * macro counts
    * macro sizes and 
    * aspect ratios
  * with boards
    * canvas sizes
    * aspect ratios
  * win conditions
    * relative importance of different density
    * routing congestion constraints
  * technical difficulty: the action space is huge and difficult to compute in time
* conclusion: automating and accelerating the chip design process can also enable co-design of AI and hardware, yielding high-performance chips customized to important workloads, such as autonomous vehicles, medical devices and data centres
* -> chip floorplanning as a learning problem

![](./pics/research/algmon.research.chip.design.google.paper.fig1.png)

* an RL agent can ben trained and modelled by a neural network that, through repeated episodes (sequences of states, actions and rewards), learns to take actions that will maximize cumulative reward (SEE the above fig)

![](./pics/research/algmon.research.chip.design.google.paper.fig2.png)

* feature embeddings
* policy and value networks

![](./pics/research/algmon.research.chip.design.google.paper.fig3.png)

* the above fig make sense but do NOT have high impact for this article

![](./pics/research/algmon.research.chip.design.google.paper.fig4.png)

* the above fig can be deeper cause we need to figure out during fine-tuning, what percentage of the network has been touched
