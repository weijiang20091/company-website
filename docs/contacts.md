---
sidebar_position: 1
---

# Algmon Contacts

* Laura Blumenschein, Purdue U, Vine Robot
* Gauri Joshi, Carnegie Mellon U, distributed learning alg
* Yooho Kim, MIT, wires in the brain
* Sharon Zhou, Stanford U, generative AI
* Alain Vaucher, IBM Research, making molecules
* Kathryn Tunyasuvunakool, Deep Mind, 3d
* Kathleen Siminyu, Mozilla Foundation, african-language data set creation
* Ishan Misra, Meta AI, train models on visual data
* Joelle Mbatchou, Regeneron Genetics Center, alg design
